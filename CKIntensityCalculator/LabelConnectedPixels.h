#pragma once

class CLabelConnectedPixels
{
protected:
	struct KEYENTRY *m_KeyLabelTable;
	unsigned short GetEquivalence(unsigned short key);
	void AddLabelEquivalence(unsigned short key, unsigned short label);
	void CleanLabelTable();
	
public:
	CLabelConnectedPixels();
	virtual ~CLabelConnectedPixels();
	bool LabelConnectedComponents(unsigned short *image, int width, int height, unsigned short threshold, int *numLabels, unsigned short **labels, int **pixelCount, unsigned short **map);
};

struct LABELENTRY
{
	unsigned short label;
	struct LABELENTRY *nextLabel;
};

struct KEYENTRY
{
	unsigned short key;
	unsigned short minLabel;
	struct LABELENTRY *labelTable;
	struct KEYENTRY *nextKey;
};
