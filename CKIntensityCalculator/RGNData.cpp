#include "stdafx.h"
#include "RGNData.h"

CRGNData::CRGNData(int index, unsigned char blue, unsigned char green, unsigned char red, int CenterX, int CenterY)
{
	m_Index = index;
	m_Blue = blue;
	m_Green = green;
	m_Red = red;
	m_CenterX = CenterX;
	m_CenterY = CenterY;
	m_RedCutoff = 0;
	m_RedContrast = 0;
	m_GreenCutoff = 0;
	m_GreenContrast = 0;
	m_BlueCutoff = 0;
	m_BlueContrast = 0;
}

CRGNData::~CRGNData()
{
}

bool CRGNData::GetOneItem(int index, unsigned char *blue, unsigned char *green, unsigned char *red, int *CenterX, int *CenterY)
{
	bool ret = false;

	if (index == m_Index)
	{
		ret = true;
		*blue = m_Blue;
		*green = m_Green;
		*red = m_Red;
		*CenterX = m_CenterX;
		*CenterY = m_CenterY;
	}

	return ret;
}

void CRGNData::SetCutoffContrast(PIXEL_COLOR_TYPE color, int cutoff, int contrast)
{
	if (color == RED_COLOR)
	{
		m_RedCutoff = cutoff;
		m_RedContrast = contrast;
	}
	else if (color == GREEN_COLOR)
	{
		m_GreenCutoff = cutoff;
		m_GreenContrast = contrast;
	}
	else
	{
		m_BlueCutoff = cutoff;
		m_BlueContrast = contrast;
	}
}

void CRGNData::GetCutoffContrast(PIXEL_COLOR_TYPE color, int *cutoff, int *contrast)
{
	if (color == RED_COLOR)
	{
		*cutoff = m_RedCutoff;
		*contrast = m_RedContrast;
	}
	else if (color == GREEN_COLOR)
	{
		*cutoff = m_GreenCutoff;
		*contrast = m_GreenContrast;
	}
	else
	{
		*cutoff = m_BlueCutoff;
		*contrast = m_BlueContrast;
	}
}