﻿
// CKIntensityCalculatorDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CKIntensityCalculator.h"
#include "CKIntensityCalculatorDlg.h"
#include "afxdialogex.h"
#include "string.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define MAXINT_12BIT 4096
#define MAXINT_14BIT 16384

// CCKIntensityCalculatorDlg dialog

CCKIntensityCalculatorDlg::CCKIntensityCalculatorDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CCKIntensityCalculatorDlg::IDD, pParent)
	, m_RegionCount(0)
	, m_DisplayedRgnIndex(0)
	, m_TIFFFilename(_T(""))
	, m_RegionFilename(_T(""))
	, m_Status(_T(""))
	, m_Grouper(NULL)
	, m_GreenRegionMap(NULL)
	, m_GreenRegionBoundary(NULL)
	, m_AveCK(0)
	, m_CKData(NULL)
	, m_GreenData(NULL)
	, m_PixelCounts(NULL)
	, m_RedPixels(NULL)
	, m_BluePixels(NULL)
	, m_BlueData(NULL)
	, m_MinPixels(0)
	, m_MaxPixels(0)
	, m_CKIntensityLabel(_T(""))
	, m_AvgCD45(0)
{
	m_MinPixels = 10;
	m_MaxPixels = 1250;
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_RegionWidth = 100;
	m_RegionHeight = 100;
	m_RedRegionImage = new unsigned short[m_RegionWidth * m_RegionHeight];
	m_GreenRegionImage = new unsigned short[m_RegionWidth * m_RegionHeight];
	m_BlueRegionImage = new unsigned short[m_RegionWidth * m_RegionHeight];
}

void CCKIntensityCalculatorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BLUE, m_Blue);
	DDX_Control(pDX, IDC_GREEN, m_Green);
	DDX_Control(pDX, IDC_RED, m_Red);
	DDX_Control(pDX, IDC_2REGION, m_2RegionDisplay);
	DDX_Control(pDX, IDC_CKIMAGE, m_CKIntensityDisplay);
	DDX_Control(pDX, IDC_RGBImage, m_RGBImageDisplay);
	DDX_Text(pDX, IDC_REGIONCOUNT, m_RegionCount);
	DDX_Control(pDX, IDC_REGIONINDEX, m_RegionIndex);
	DDX_Text(pDX, IDC_OVERLAY, m_TIFFFilename);
	DDX_Text(pDX, IDC_REGION, m_RegionFilename);
	DDX_Text(pDX, IDC_STATUS, m_Status);
	DDX_Control(pDX, IDC_INTENSITYLIST, m_IntensityList);
	DDX_Text(pDX, IDC_AVECK, m_AveCK);
	DDX_Control(pDX, IDC_CTCONLY, m_CTCOnly);
	DDX_Control(pDX, IDC_WBCONLY, m_WBCOnly);
	DDX_Control(pDX, IDC_ALLTYPES, m_Alltypes);
	DDX_Control(pDX, IDC_SRED, m_SearchRed);
	DDX_Control(pDX, IDC_SGREEN, m_SearchGreen);
	DDX_Control(pDX, IDC_SBLUE, m_SearchBlue);
	DDX_Text(pDX, IDC_MINPIXEL, m_MinPixels);
	DDV_MinMaxInt(pDX, m_MinPixels, 10, 800);
	DDX_Text(pDX, IDC_MAXPIXEL, m_MaxPixels);
	DDV_MinMaxInt(pDX, m_MaxPixels, 20, 2500);
	DDX_Text(pDX, IDC_CKINTENLABEL, m_CKIntensityLabel);
	DDX_Control(pDX, IDC_LEICA, m_Leica);
	DDX_Control(pDX, IDC_ZEISS, m_Zeiss);
	DDX_Text(pDX, IDC_AVERAGECD45, m_AvgCD45);
}

BEGIN_MESSAGE_MAP(CCKIntensityCalculatorDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_OPENTIFF, &CCKIntensityCalculatorDlg::OnBnClickedOpentiff)
	ON_BN_CLICKED(IDC_OPENRGN, &CCKIntensityCalculatorDlg::OnBnClickedOpenrgn)
	ON_BN_CLICKED(IDC_SELECT, &CCKIntensityCalculatorDlg::OnBnClickedSelect)
	ON_BN_CLICKED(IDC_NEXT, &CCKIntensityCalculatorDlg::OnBnClickedNext)
	ON_BN_CLICKED(IDC_PREV, &CCKIntensityCalculatorDlg::OnBnClickedPrev)
	ON_BN_CLICKED(IDC_BLUE, &CCKIntensityCalculatorDlg::OnBnClickedBlue)
	ON_BN_CLICKED(IDC_GREEN, &CCKIntensityCalculatorDlg::OnBnClickedGreen)
	ON_BN_CLICKED(IDC_RED, &CCKIntensityCalculatorDlg::OnBnClickedRed)
	ON_BN_CLICKED(IDCANCEL, &CCKIntensityCalculatorDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_CALCULATE, &CCKIntensityCalculatorDlg::OnBnClickedCalculate)
	ON_NOTIFY(NM_DBLCLK, IDC_INTENSITYLIST, &CCKIntensityCalculatorDlg::OnNMDblclkIntensitylist)
	ON_BN_CLICKED(IDC_EXPORT, &CCKIntensityCalculatorDlg::OnBnClickedExport)
END_MESSAGE_MAP()


// CCKIntensityCalculatorDlg message handlers

BOOL CCKIntensityCalculatorDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	m_Log.NewLog(_T("CKIntensityCalculator (v2.9)"));
	m_Blue.SetCheck(TRUE);
	m_Green.SetCheck(TRUE);
	m_Red.SetCheck(TRUE);
	m_CKIntensityLabel = _T("CK Intensity (Red Intensity) Image");
	m_Status = "Please load Red Channel TIFF File";
	UpdateData(FALSE);
	LoadDefaultSettings();
	m_RedTIFFData = new CSingleChannelTIFFData();
	m_GreenTIFFData = new CSingleChannelTIFFData();
	m_BlueTIFFData = new CSingleChannelTIFFData();
	m_ImageWidth = 0;
	m_ImageHeight = 0;
	m_RegionIndex.SetWindowTextW(_T("0"));
	// TODO: Add extra initialization here
	int nSize[] = {60, 70, 75, 75, 75, 70, 70};
	LV_COLUMN nListColumn;
	for (int i = 0; i < 7; i++)
	{
		nListColumn.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
		nListColumn.fmt = LVCFMT_LEFT;
		nListColumn.cx = nSize[i];
		nListColumn.iSubItem = 0;
		if (i==0)
			nListColumn.pszText = _T("RgnIdx");
		else if (i==1)
			nListColumn.pszText = _T("RPixels");
		else if (i==2)
			nListColumn.pszText = _T("RIntensity");
		else if (i==3)
			nListColumn.pszText = _T("GPixels");
		else if (i == 4)
			nListColumn.pszText = _T("GIntensity");
		else if (i == 5)
			nListColumn.pszText = _T("BPixels");
		else
			nListColumn.pszText = _T("BIntensity");
		m_IntensityList.InsertColumn(i, &nListColumn);
	}
	m_IntensityList.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	m_Grouper = new CLabelConnectedPixels();
	m_Alltypes.SetCheck(BST_CHECKED);
	m_SearchGreen.SetCheck(BST_CHECKED);
	m_Leica.SetCheck(BST_CHECKED);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCKIntensityCalculatorDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		if (m_Image != NULL)
		{
			CPaintDC dc(&m_RGBImageDisplay);
			CRect rect;
			m_RGBImageDisplay.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_Image.StretchBlt(dc.m_hDC, rect);
		}
		if (m_CellRegionImage != NULL)
		{
			CPaintDC dc(&m_2RegionDisplay);
			CRect rect;
			m_2RegionDisplay.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_CellRegionImage.StretchBlt(dc.m_hDC, rect);
		}
		if (m_CKRegionImage != NULL)
		{
			CPaintDC dc(&m_CKIntensityDisplay);
			CRect rect;
			m_CKIntensityDisplay.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_CKRegionImage.StretchBlt(dc.m_hDC, rect);
		}
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCKIntensityCalculatorDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CCKIntensityCalculatorDlg::OnBnClickedOpentiff()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	UpdateData(TRUE);
	CButton *btn = (CButton *)GetDlgItem(IDC_OPENTIFF);
	btn->EnableWindow(FALSE);
	if (m_Image != NULL)
	{
		m_Image.Destroy();
	}
	if (m_CKRegionImage != NULL)
	{
		m_CKRegionImage.Destroy();
	}
	if (m_CellRegionImage != NULL)
	{
		m_CellRegionImage.Destroy();
	}
	FreeImages();
	m_TIFFFilename = _T("");
	m_RegionFilename = _T("");
	FreeRGNData();
	m_RegionCount = 0;
	m_DisplayedRgnIndex = 0;
	m_RegionIndex.SetWindowTextW(_T("0"));
	m_IntensityList.DeleteAllItems();
	Invalidate(TRUE);
	m_Status = _T("Start to load TIFF Files. Please wait...");
	UpdateData(FALSE);

	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("TIFF files (*.tif)|*.tif||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		m_TIFFFilename = dlg.GetFileName();
		CString filename = dlg.GetPathName();
		CString m_FullPathTIFFFilename = filename;
		bool ret = m_RedTIFFData->LoadRawTIFFFile(filename);
		if (ret)
		{
			ret = false;
			int index = m_TIFFFilename.Find(Leica_Red_Prefix, 0);
			if (index == -1)
			{
				int postfixIndex = filename.Find(Zeiss_Red_Postfix);
				if (postfixIndex == -1)
				{
					m_Status.Format(_T("Red TIFF Filename %s doesn't have PREFIX %s or POSTFIX %s"), m_TIFFFilename, Leica_Red_Prefix, Zeiss_Red_Postfix);
					UpdateData(FALSE);
				}
				else
				{
					CString samplePathName = filename.Mid(0, postfixIndex);
					CString filename2 = samplePathName + Zeiss_Green_Postfix;
					ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
					if (ret)
					{
						filename2 = samplePathName + Zeiss_Blue_Postfix;
						ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
						if (ret)
						{
							m_ZeissData = true;
						}
					}
				}
			}
			else
			{
				CString postfix = m_TIFFFilename.Mid(Leica_Red_Prefix.GetLength());
				index = filename.Find(Leica_Red_Prefix, 0);
				CString filename1 = filename.Mid(0, index);
				CString filename2;
				filename2.Format(_T("%s%s%s"), filename1, Leica_Green_Prefix, postfix);
				ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
				if (ret)
				{
					filename2.Format(_T("%s%s%s"), filename1, Leica_Blue_Prefix, postfix);
					ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
					if (ret)
					{
						m_ZeissData = false;
					}
				}
			}
		}
		if (ret)
		{
			if (m_Image != NULL)
			{
				m_Image.Destroy();
				m_ImageWidth = 0;
				m_ImageHeight = 0;
			}

			m_ImageWidth = m_RedTIFFData->GetSubsampledWidth();
			m_ImageHeight = m_RedTIFFData->GetSubsampledHeight();
			m_Image.Create(m_ImageWidth, -m_ImageHeight, 24);
			unsigned short *blue = new unsigned short[m_ImageWidth * m_ImageHeight];
			unsigned short *green = new unsigned short[m_ImageWidth * m_ImageHeight];
			unsigned short *red = new unsigned short[m_ImageWidth * m_ImageHeight];
			ret = m_RedTIFFData->GetSubsampleImage(red);
			if (ret)
			{
				ret = m_GreenTIFFData->GetSubsampleImage(green);
				if (ret)
				{
					ret = m_BlueTIFFData->GetSubsampleImage(blue);
					if (ret)
					{
						m_Status = _T("Please load RGN File.");
						UpdateData(FALSE);
						CopyToRGBImage((m_Blue.GetCheck() ? blue : NULL), (m_Green.GetCheck() ? green : NULL), (m_Red.GetCheck() ? red : NULL),
							m_RedTIFFData->GetCPI(), 900, m_GreenTIFFData->GetCPI(), 1100, m_BlueTIFFData->GetCPI(), 1200);
						delete[] red;
						delete[] green;
						delete[] blue;
						RECT rect;
						m_RGBImageDisplay.GetWindowRect(&rect);
						ScreenToClient(&rect);
						InvalidateRect(&rect, false);
					}
				}
			}
			else
			{
				m_Status = _T("Failed to get SubSampledImage");
				UpdateData(FALSE);
			}
		}
	}
	btn = (CButton *)GetDlgItem(IDC_OPENTIFF);
	btn->EnableWindow(TRUE);
}


void CCKIntensityCalculatorDlg::OnBnClickedOpenrgn()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	UpdateData(TRUE);
	FreeRGNData();
	m_RegionCount = 0;
	m_DisplayedRgnIndex = 0;
	m_RegionIndex.SetWindowTextW(_T("0"));
	UpdateData(FALSE);

	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("Region files (*.rgn, *.cz)|*.rgn; *.cz"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		CString RGNpathname = dlg.GetPathName();
		m_RegionFilename = dlg.GetFileName();
		if (LoadRegionData(RGNpathname))
		{
			m_Status = _T("Region No.1 Image is Displayed");
			m_RegionCount = (int)m_RGNDataArray.size();
			m_DisplayedRgnIndex = 1;
			CString indexStr;
			indexStr.Format(_T("%d"), m_DisplayedRgnIndex);
			m_RegionIndex.SetWindowTextW(indexStr);
			BYTE blue;
			BYTE green;
			BYTE red;
			int CenterX;
			int CenterY;
			BOOL ret = m_RGNDataArray[m_DisplayedRgnIndex - 1]->GetOneItem(m_DisplayedRgnIndex, &blue, &green, &red, &CenterX, &CenterY);
			if (!ret)
			{
				m_Status = _T("Failed to get RGN Data from RGN Data Array");
				UpdateData(FALSE);
			}
			else
			{
				DisplayROI(m_DisplayedRgnIndex);
			}
		}
		else
		{
			m_Status.Format(_T("Failed to open %s"), m_RegionFilename);
			UpdateData(FALSE);
		}
	}
}

void CCKIntensityCalculatorDlg::OnBnClickedSelect()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CString indexStr;
	m_RegionIndex.GetWindowTextW(indexStr);
	int newIndex = _wtoi(indexStr);
	if ((newIndex > 0) && (newIndex <= m_RegionCount))
	{
		m_DisplayedRgnIndex = newIndex;
		indexStr.Format(_T("%d"), m_DisplayedRgnIndex);
		m_RegionIndex.SetWindowTextW(indexStr);
		DisplayROI(m_DisplayedRgnIndex);
		UpdateData(FALSE);
	}
	else
	{
		indexStr.Format(_T("Rgn Index has to be larger than 0 and less than %d."), m_RegionCount + 1);
		AfxMessageBox(indexStr);
		indexStr.Format(_T("%d"), m_DisplayedRgnIndex);
		m_RegionIndex.SetWindowTextW(indexStr);
	}
}

void CCKIntensityCalculatorDlg::OnBnClickedNext()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	if ((m_DisplayedRgnIndex + 1) <= m_RegionCount)
	{
		m_DisplayedRgnIndex++;
		CString indexStr;
		indexStr.Format(_T("%d"), m_DisplayedRgnIndex);
		m_RegionIndex.SetWindowTextW(indexStr);
		DisplayROI(m_DisplayedRgnIndex);
		UpdateData(FALSE);
	}
}


void CCKIntensityCalculatorDlg::OnBnClickedPrev()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	if ((m_DisplayedRgnIndex > 1) && ((m_DisplayedRgnIndex - 1) <= m_RegionCount))
	{
		m_DisplayedRgnIndex--;
		CString indexStr;
		indexStr.Format(_T("%d"), m_DisplayedRgnIndex);
		m_RegionIndex.SetWindowTextW(indexStr);
		DisplayROI(m_DisplayedRgnIndex);
		UpdateData(FALSE);
	}
}


void CCKIntensityCalculatorDlg::OnBnClickedBlue()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	ClickColor();
}


void CCKIntensityCalculatorDlg::OnBnClickedGreen()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	ClickColor();
}


void CCKIntensityCalculatorDlg::OnBnClickedRed()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	ClickColor();
}

void CCKIntensityCalculatorDlg::FreeImages()
{
	if (m_GreenRegionMap != NULL)
	{
		delete[] m_GreenRegionMap;
		m_GreenRegionMap = NULL;
	}
	if (m_GreenRegionBoundary != NULL)
	{
		delete[] m_GreenRegionBoundary;
		m_GreenRegionBoundary = NULL;
	}
}

void CCKIntensityCalculatorDlg::OnBnClickedCancel()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	delete m_RedRegionImage;
	delete m_GreenRegionImage;
	delete m_BlueRegionImage;
	delete m_RedTIFFData;
	delete m_GreenTIFFData;
	delete m_BlueTIFFData;
	FreeRGNData();
	if (m_Grouper != NULL)
	{
		delete m_Grouper;
		m_Grouper = NULL;
	}
	FreeImages();
	if (m_CKData != NULL)
	{
		delete[] m_CKData;
		m_CKData = NULL;
	}
	if (m_GreenData != NULL)
	{
		delete[] m_GreenData;
		m_GreenData = NULL;
	}
	if (m_PixelCounts != NULL)
	{
		delete[] m_PixelCounts;
		m_PixelCounts = NULL;
	}
	if (m_RedPixels != NULL)
	{
		delete[] m_RedPixels;
		m_RedPixels = NULL;
	}
	if (m_BluePixels != NULL)
	{
		delete[] m_BluePixels;
		m_BluePixels = NULL;
	}
	if (m_BlueData != NULL)
	{
		delete[] m_BlueData;
		m_BlueData = NULL;
	}
	m_IntensityList.DeleteAllItems();
	CDialogEx::OnCancel();
}

void CCKIntensityCalculatorDlg::CopyToRGBImage(unsigned short *pBlueBuffer, unsigned short *pGreenBuffer, unsigned short *pRedBuffer,
	int redCutoff, int redContrast, int greenCutoff, int greenContrast, int blueCutoff, int blueContrast)
{
	if (m_Image != NULL)
	{
		BYTE *pCursor = (BYTE*)m_Image.GetBits();
		int nHeight = m_ImageHeight;
		int nWidth = m_ImageWidth;
		int nStride = m_Image.GetPitch() - (nWidth * 3);

		for (int y = 0; y<nHeight; y++)
		{
			for (int x = 0; x<nWidth; x++)
			{
				if (pBlueBuffer != NULL)
					*pCursor++ = GetContrastEnhancedByte(*pBlueBuffer++, blueCutoff, blueContrast);
				else
					*pCursor++ = (BYTE)0;
				if (pGreenBuffer != NULL)
					*pCursor++ = GetContrastEnhancedByte(*pGreenBuffer++, greenCutoff, greenContrast);
				else
					*pCursor++ = (BYTE)0;
				if (pRedBuffer != NULL)
					*pCursor++ = GetContrastEnhancedByte(*pRedBuffer++, redCutoff, redContrast);
				else
					*pCursor++ = (BYTE)0;
			}
			if (nStride > 0)
				pCursor += nStride;
		}
	}
}

void CCKIntensityCalculatorDlg::FreeRGNData(void)
{
	if (m_RGNDataArray.size() != 0)
	{
		for (int i = 0; i < (int)m_RGNDataArray.size(); i++)
		{
			delete m_RGNDataArray[i];
			m_RGNDataArray[i] = NULL;
		}
		m_RGNDataArray.clear();
	}
}

BOOL CCKIntensityCalculatorDlg::DisplayROI(int index)
{
	BOOL ret = FALSE;
	BYTE blue1;
	BYTE green1;
	BYTE red1;
	int index1 = index - 1;
	int CenterX;
	int CenterY;
	CString message;

	ret = m_RGNDataArray[index1]->GetOneItem(index, &blue1, &green1, &red1, &CenterX, &CenterY);
	if (!ret)
	{
		message.Format(_T("Failed to get Rgn No.%d"), index);
		m_Status = message;
		UpdateData(FALSE);
	}
	else
	{
		m_RgnIdx = index1;
		int width = 100;
		m_RegionX0 = CenterX - 50;
		m_RegionY0 = CenterY - 50;
		bool redRet = m_RedTIFFData->GetImageRegion(m_RegionX0, m_RegionY0, width, width, m_RedRegionImage);
		bool greenRet = m_GreenTIFFData->GetImageRegion(m_RegionX0, m_RegionY0, width, width, m_GreenRegionImage);
		bool blueRet = m_BlueTIFFData->GetImageRegion(m_RegionX0, m_RegionY0, width, width, m_BlueRegionImage);
		if (!redRet || !greenRet || !blueRet)
		{
			message.Format(_T("Failed to get ROI Image Data for Rgn No.%d"), index);
			m_Status = message;
			UpdateData(FALSE);
		}
		else
		{
			int redCutoff = 0;
			int redContrast = 0;
			int greenCutoff = 0;
			int greenContrast = 0;
			int blueCutoff = 0;
			int blueContrast = 0;
			m_RGNDataArray[m_RgnIdx]->GetCutoffContrast(RED_COLOR, &redCutoff, &redContrast);
			if ((redCutoff == 0) || (redContrast == 0))
			{
				if (m_Leica.GetCheck() == BST_CHECKED)
				{
					redCutoff = 300;
					redContrast = 400;
				}
				else
				{
					redCutoff = 550;
					redContrast = 1500;
				}
			}
			m_RGNDataArray[m_RgnIdx]->GetCutoffContrast(GREEN_COLOR, &greenCutoff, &greenContrast);
			if ((greenCutoff == 0) || (greenContrast == 0))
			{
				if (m_Leica.GetCheck() == BST_CHECKED)
				{
					greenCutoff = 400;
					greenContrast = 1200;
				}
				else
				{
					greenCutoff = 250;
					greenContrast = 1500;
				}
			}
			m_RGNDataArray[m_RgnIdx]->GetCutoffContrast(BLUE_COLOR, &blueCutoff, &blueContrast);
			if ((blueCutoff == 0) || (blueContrast == 0))
			{
				if (m_Leica.GetCheck() == BST_CHECKED)
				{
					blueCutoff = 350;
					blueContrast = 1500;
				}
				else
				{
					blueCutoff = 300;
					blueContrast = 6000;
				}
			}
			
			if (m_Image != NULL)
			{
				m_Image.Destroy();
				m_ImageWidth = 0;
				m_ImageHeight = 0;
			}
			m_ImageWidth = width;
			m_ImageHeight = width;
			m_Image.Create(m_ImageWidth, -m_ImageHeight, 24);

			CopyToRGBImage((m_Blue.GetCheck() ? m_BlueRegionImage : NULL), (m_Green.GetCheck() ? m_GreenRegionImage : NULL), (m_Red.GetCheck() ? m_RedRegionImage : NULL),
				redCutoff, redContrast, greenCutoff, greenContrast, blueCutoff, blueContrast);

			RECT rect1;
			m_RGBImageDisplay.GetWindowRect(&rect1);
			ScreenToClient(&rect1);
			InvalidateRect(&rect1, false);
			m_Status.Format(_T("Displayed ROI Image of Rgn No.%d, UpLeftCorner: X=%d,Y=%d"), index,
				CenterX - (width / 2), CenterY - (width / 2));
			UpdateData(FALSE);
		}
	}
	return ret;
}

void CCKIntensityCalculatorDlg::ClickColor()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	if ((m_RegionCount == 0) && (m_DisplayedRgnIndex == 0))
	{
		if (m_Image != NULL)
		{
			m_Image.Destroy();
			m_ImageWidth = 0;
			m_ImageHeight = 0;
		}
		m_ImageWidth = m_RedTIFFData->GetSubsampledWidth();
		m_ImageHeight = m_RedTIFFData->GetSubsampledHeight();
		m_Image.Create(m_ImageWidth, -m_ImageHeight, 24);
		unsigned short *blue = new unsigned short[m_ImageWidth * m_ImageHeight];
		unsigned short *green = new unsigned short[m_ImageWidth * m_ImageHeight];
		unsigned short *red = new unsigned short[m_ImageWidth * m_ImageHeight];
		bool ret = m_RedTIFFData->GetSubsampleImage(red);
		if (ret)
		{
			ret = m_GreenTIFFData->GetSubsampleImage(green);
			if (ret)
			{
				ret = m_BlueTIFFData->GetSubsampleImage(blue);
				if (ret)
				{
					CopyToRGBImage((m_Blue.GetCheck() ? blue : NULL), (m_Green.GetCheck() ? green : NULL), (m_Red.GetCheck() ? red : NULL),
						m_RedTIFFData->GetCPI(), 900, m_GreenTIFFData->GetCPI(), 1100, m_BlueTIFFData->GetCPI(), 1200);
					delete[] red;
					delete[] green;
					delete[] blue;
					RECT rect;
					m_RGBImageDisplay.GetWindowRect(&rect);
					ScreenToClient(&rect);
					InvalidateRect(&rect, false);
				}
			}
		}
		else
		{
			m_Status = _T("Failed to get SubSampledImage");
			UpdateData(FALSE);
		}
	}
	else
	{
		if (m_Image != NULL)
		{
			m_Image.Destroy();
			m_ImageWidth = 0;
			m_ImageHeight = 0;
		}
		int width = 100;
		m_ImageWidth = 100;
		m_ImageHeight = 100;
		m_Image.Create(m_ImageWidth, -m_ImageHeight, 24);

		BOOL ret = FALSE;
		bool redRet = m_RedTIFFData->GetImageRegion(m_RegionX0, m_RegionY0, width, width, m_RedRegionImage);
		bool greenRet = m_GreenTIFFData->GetImageRegion(m_RegionX0, m_RegionY0, width, width, m_GreenRegionImage);
		bool blueRet = m_BlueTIFFData->GetImageRegion(m_RegionX0, m_RegionY0, width, width, m_BlueRegionImage);
		if (redRet && greenRet && blueRet)
			ret = TRUE;
		if (ret)
		{
			int redCutoff = 0;
			int redContrast = 0;
			int greenCutoff = 0;
			int greenContrast = 0;
			int blueCutoff = 0;
			int blueContrast = 0;
			m_RGNDataArray[m_RgnIdx]->GetCutoffContrast(RED_COLOR, &redCutoff, &redContrast);
			if ((redCutoff == 0) || (redContrast == 0))
			{
				if (m_Leica.GetCheck() == BST_CHECKED)
				{
					redCutoff = 340;
					redContrast = 450;
				}
				else
				{
					redCutoff = 550;
					redContrast = 1500;
				}
			}
			m_RGNDataArray[m_RgnIdx]->GetCutoffContrast(GREEN_COLOR, &greenCutoff, &greenContrast);
			if ((greenCutoff == 0) || (greenContrast == 0))
			{
				if (m_Leica.GetCheck() == BST_CHECKED)
				{
					greenCutoff = 400;
					greenContrast = 1200;
				}
				else
				{
					greenCutoff = 250;
					greenContrast = 1500;
				}
			}
			m_RGNDataArray[m_RgnIdx]->GetCutoffContrast(BLUE_COLOR, &blueCutoff, &blueContrast);
			if ((blueCutoff == 0) || (blueContrast == 0))
			{
				if (m_Leica.GetCheck() == BST_CHECKED)
				{
					blueCutoff = 350;
					blueContrast = 1600;
				}
				else
				{
					blueCutoff = 300;
					blueContrast = 6000;
				}
			}
			CopyToRGBImage((m_Blue.GetCheck() ? m_BlueRegionImage : NULL), (m_Green.GetCheck() ? m_GreenRegionImage : NULL), (m_Red.GetCheck() ? m_RedRegionImage : NULL),
				redCutoff, redContrast, greenCutoff, greenContrast, blueCutoff, blueContrast);

			RECT rect;
			m_RGBImageDisplay.GetWindowRect(&rect);
			ScreenToClient(&rect);
			InvalidateRect(&rect, false);
		}
		else
		{
			m_Status = _T("Failed to get Image Data");
			UpdateData(FALSE);
		}
	}
}

void CCKIntensityCalculatorDlg::OnBnClickedCalculate()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	UpdateData(TRUE);
	if (m_CKData != NULL)
	{
		delete[] m_CKData;
		m_CKData = NULL;
	}
	if (m_GreenData != NULL)
	{
		delete[] m_GreenData;
		m_GreenData = NULL;
	}
	if (m_PixelCounts != NULL)
	{
		delete[] m_PixelCounts;
		m_PixelCounts = NULL;
	}
	if (m_RedPixels != NULL)
	{
		delete[] m_RedPixels;
		m_RedPixels = NULL;
	}
	if (m_BluePixels != NULL)
	{
		delete[] m_BluePixels;
		m_BluePixels = NULL;
	}
	if (m_BlueData != NULL)
	{
		delete[] m_BlueData;
		m_BlueData = NULL;
	}
	CleanIntensityList();
	m_AveCK = 0;
	m_AvgCD45 = 0;
	UpdateData(FALSE);
	m_CKData = new int[m_RGNDataArray.size()];
	m_GreenData = new int[m_RGNDataArray.size()];
	m_PixelCounts = new int[m_RGNDataArray.size()];
	m_RedPixels = new int[m_RGNDataArray.size()];
	m_BluePixels = new int[m_RGNDataArray.size()];
	m_BlueData = new int[m_RGNDataArray.size()];
	int itemIndex = 0;
	int width = 100;
	int lastIndex = -1;
	for (int i = 0; i < (int)m_RGNDataArray.size(); i++)
	{
		m_RgnIdx = i; 
		m_CKData[i] = 0;
		m_GreenData[i] = 0;
		m_PixelCounts[i] = 0;
		m_RedPixels[i] = 0;
		m_BluePixels[i] = 0;
		m_BlueData[i] = 0;
		unsigned char blue;
		unsigned char green;
		unsigned char red;
		int CenterX;
		int CenterY;
		bool ret = m_RGNDataArray[i]->GetOneItem(i + 1, &blue, &green, &red, &CenterX, &CenterY);
		if (ret)
		{
			if (m_CTCOnly.GetCheck() == BST_CHECKED)
			{
				if ((red != 255) || (green != 0))
					continue;
			}
			else if (m_WBCOnly.GetCheck() == BST_CHECKED)
			{
				if ((red != 0) || (green != 255))
					continue;
			}
			m_RegionX0 = CenterX - 50;
			m_RegionY0 = CenterY - 50;
			BOOL ret1 = FALSE;
			bool redRet = m_RedTIFFData->GetImageRegion(m_RegionX0, m_RegionY0, width, width, m_RedRegionImage);
			bool greenRet = m_GreenTIFFData->GetImageRegion(m_RegionX0, m_RegionY0, width, width, m_GreenRegionImage);
			bool blueRet = m_BlueTIFFData->GetImageRegion(m_RegionX0, m_RegionY0, width, width, m_BlueRegionImage);
			if (redRet && greenRet && blueRet)
				ret1 = TRUE;
			if (!ret1)
			{
				m_Status.Format(_T("Failed to get Region Image for Region No.%d"), i + 1);
				UpdateData(FALSE);
				break;
			}
			else
			{
				lastIndex = i;
				FreeImages();
				m_GreenRegionMap = new unsigned char[width * width];
				memset(m_GreenRegionMap, 0, width * width);
				m_GreenRegionBoundary = new unsigned char[width * width];
				memset(m_GreenRegionBoundary, 0, width * width);
				int numPixels = 0;
				int sum = 0;
				ret = GetRegionPixelMap(m_GreenRegionMap, &numPixels);
				if (m_SearchGreen.GetCheck() == BST_CHECKED)
				{
					m_PixelCounts[i] = numPixels;
					m_RedPixels[i] = SumPixelCount(m_GreenRegionMap, width, width, m_RedRegionImage);
					m_BluePixels[i] = SumPixelCount(m_GreenRegionMap, width, width, m_BlueRegionImage);
				}
				else if (m_SearchRed.GetCheck() == BST_CHECKED)
				{
					m_RedPixels[i] = numPixels;
					m_PixelCounts[i] = SumPixelCount(m_GreenRegionMap, width, width, m_GreenRegionImage);
					m_BluePixels[i] = SumPixelCount(m_GreenRegionMap, width, width, m_BlueRegionImage);
				}
				else if (m_SearchBlue.GetCheck() == BST_CHECKED)
				{
					m_BluePixels[i] = numPixels;
					m_RedPixels[i] = SumPixelCount(m_GreenRegionMap, width, width, m_RedRegionImage);
					m_PixelCounts[i] = SumPixelCount(m_GreenRegionMap, width, width, m_GreenRegionImage);
				}
					
				if (ret)
				{
					memcpy(m_GreenRegionBoundary, m_GreenRegionMap, width * width);
					PaintCellRegionBoundary(m_GreenRegionBoundary, width, width);
					SumCKIntensity(m_RedRegionImage, m_GreenRegionMap, width, width, &sum);
					m_CKData[i] = sum;
					SumCKIntensity(m_GreenRegionImage, m_GreenRegionMap, width, width, &sum);
					m_GreenData[i] = sum;
					SumCKIntensity(m_BlueRegionImage, m_GreenRegionMap, width, width, &sum);
					m_BlueData[i] = sum;
					CString labelStr;
					labelStr.Format(_T("%d"), i + 1);
					int idx = m_IntensityList.InsertItem(itemIndex++, labelStr);
					labelStr.Format(_T("%d"), m_RedPixels[i]);
					m_IntensityList.SetItemText(idx, 1, labelStr);
					labelStr.Format(_T("%d"), m_CKData[i]);
					m_IntensityList.SetItemText(idx, 2, labelStr);
					labelStr.Format(_T("%d"), numPixels);
					m_IntensityList.SetItemText(idx, 3, labelStr);
					labelStr.Format(_T("%d"), m_GreenData[i]);
					m_IntensityList.SetItemText(idx, 4, labelStr);
					labelStr.Format(_T("%d"), m_BluePixels[i]);
					m_IntensityList.SetItemText(idx, 5, labelStr);
					labelStr.Format(_T("%d"), m_BlueData[i]);
					m_IntensityList.SetItemText(idx, 6, labelStr);
				}
				else
				{
					lastIndex = -1;
					CString labelStr;
					labelStr.Format(_T("%d"), i + 1);
					int idx = m_IntensityList.InsertItem(itemIndex++, labelStr);
					labelStr.Format(_T("%d"), -1);
					m_IntensityList.SetItemText(idx, 1, labelStr);
					labelStr.Format(_T("%d"), -1);
					m_IntensityList.SetItemText(idx, 2, labelStr);
					labelStr.Format(_T("%d"), -1);
					m_IntensityList.SetItemText(idx, 3, labelStr);
					labelStr.Format(_T("%d"), -1);
					m_IntensityList.SetItemText(idx, 4, labelStr);
					labelStr.Format(_T("%d"), -1);
					m_IntensityList.SetItemText(idx, 5, labelStr);
					labelStr.Format(_T("%d"), -1);
					m_IntensityList.SetItemText(idx, 6, labelStr);
					m_CKData[i] = 0;
					m_GreenData[i] = 0;
					m_PixelCounts[i] = 0;
					m_RedPixels[i] = 0;
					m_BluePixels[i] = 0;
					m_BlueData[i] = 0;
					m_Status.Format(_T("Failed to create Region Map for Region No.%d"), i + 1);
					UpdateData(FALSE);
				}
			}
		}
		else
		{
			m_Status.Format(_T("Failed to get Region Data for Region No.%d"), i+1);
			UpdateData(FALSE);
			break;
		}
	}

	if (lastIndex > -1)
	{
		m_RgnIdx = lastIndex;
		CopyGreenImage(m_GreenRegionMap, width, width);
		RECT rect;
		m_2RegionDisplay.GetWindowRect(&rect);
		ScreenToClient(&rect);
		InvalidateRect(&rect, false);
		CopyCKImage(m_GreenRegionBoundary);
		m_CKIntensityDisplay.GetWindowRect(&rect);
		ScreenToClient(&rect);
		InvalidateRect(&rect, false);
		m_DisplayedRgnIndex = lastIndex + 1;
		CString indexStr;
		indexStr.Format(_T("%d"), m_DisplayedRgnIndex);
		m_RegionIndex.SetWindowTextW(indexStr);
		DisplayROI(m_DisplayedRgnIndex);
		UpdateData(FALSE);
	}
	
	unsigned long totalPixels = 0;
	unsigned long totalCKs = 0;
	unsigned long totalCDPixels = 0;
	unsigned long totalCDs = 0;
	for (int i = 0; i < (int)m_RGNDataArray.size(); i++)
	{
		if (m_SearchGreen.GetCheck() == BST_CHECKED)
			totalPixels += m_PixelCounts[i];
		else if (m_SearchRed.GetCheck() == BST_CHECKED)
			totalPixels += m_RedPixels[i];
		else 
			totalPixels += m_BluePixels[i];
		totalCKs += m_CKData[i];
		totalCDPixels += m_PixelCounts[i];
		totalCDs += m_GreenData[i];
	}
	if (totalCDPixels > 0)
	{
		m_AvgCD45 = totalCDs / totalCDPixels;
		UpdateData(FALSE);
	}
	if (totalPixels > 0)
	{
		m_AveCK = totalCKs / totalPixels;
		UpdateData(FALSE);
	} 
}


void CCKIntensityCalculatorDlg::OnNMDblclkIntensitylist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO:  在此加入控制項告知處理常式程式碼
	int nSel = m_IntensityList.GetNextItem(-1, LVNI_ALL | LVNI_SELECTED);
	CString text = m_IntensityList.GetItemText(nSel, 0);
	int itemData = _wtoi(text);

	unsigned char blue;
	unsigned char green;
	unsigned char red;
	int CenterX;
	int CenterY;
	m_RgnIdx = itemData - 1;
	bool ret = m_RGNDataArray[m_RgnIdx]->GetOneItem(itemData, &blue, &green, &red, &CenterX, &CenterY);
	if (ret)
	{
		int width = 100;
		m_RegionX0 = CenterX - 50;
		m_RegionY0 = CenterY - 50;
		BOOL ret1 = FALSE;
		bool redRet = m_RedTIFFData->GetImageRegion(m_RegionX0, m_RegionY0, width, width, m_RedRegionImage);
		bool greenRet = m_GreenTIFFData->GetImageRegion(m_RegionX0, m_RegionY0, width, width, m_GreenRegionImage);
		bool blueRet = m_BlueTIFFData->GetImageRegion(m_RegionX0, m_RegionY0, width, width, m_BlueRegionImage);
		if (redRet && greenRet && blueRet)
			ret1 = TRUE;
		if (!ret1)
		{
			m_Status.Format(_T("Failed to get Region Image for Region No.%d"), itemData);
			UpdateData(FALSE);
		}
		else
		{
			FreeImages();
			m_GreenRegionMap = new unsigned char[width * width];
			memset(m_GreenRegionMap, 0, width * width);
			m_GreenRegionBoundary = new unsigned char[width * width];
			memset(m_GreenRegionBoundary, 0, width * width);
			int numPixels;
			ret = GetRegionPixelMap(m_GreenRegionMap, &numPixels);
			if (ret)
			{
				memcpy(m_GreenRegionBoundary, m_GreenRegionMap, width * width);
				PaintCellRegionBoundary(m_GreenRegionBoundary, width, width);
				CopyGreenImage(m_GreenRegionMap, width, width);
				RECT rect;
				m_2RegionDisplay.GetWindowRect(&rect);
				ScreenToClient(&rect);
				InvalidateRect(&rect, false);
				CopyCKImage(m_GreenRegionBoundary);
				m_CKIntensityDisplay.GetWindowRect(&rect);
				ScreenToClient(&rect);
				InvalidateRect(&rect, false);
				m_DisplayedRgnIndex = itemData;
				CString indexStr;
				indexStr.Format(_T("%d"), m_DisplayedRgnIndex);
				m_RegionIndex.SetWindowTextW(indexStr);
				DisplayROI(m_DisplayedRgnIndex);
				UpdateData(FALSE);
			}
			else
			{
				m_Status.Format(_T("Failed to create Region Map for Region No.%d"), itemData);
				UpdateData(FALSE);
			}
		}
	}
	else
	{
		m_Status.Format(_T("Failed to get Region Data for Region No.%d"), itemData);
		UpdateData(FALSE);
	}
	*pResult = 0;
}

bool CCKIntensityCalculatorDlg::GetRegionPixelMap(unsigned char *map, int *numPixels)
{
	bool ret = false;
	CString message;

	if ((m_WBCOnly.GetCheck() == BST_CHECKED) && (m_SearchRed.GetCheck() == BST_CHECKED))
	{
		m_SearchRed.SetCheck(BST_UNCHECKED);
		m_SearchGreen.SetCheck(BST_CHECKED);
	}
	
	struct HitImageParams *params = new struct HitImageParams;
	memset(params, 0, sizeof(struct HitImageParams));

	m_ImageWidth = m_RegionWidth;
	m_ImageHeight = m_RegionHeight;

	while (true)
	{
		if (NULL == (params->redChannel = GetImageParams(m_RedRegionImage, m_ImageWidth, m_ImageHeight)))
		{
			break;
		}

		message.Format(_T("Red Parameters: Cutoff=%d, Contrast=%d, NumLabels=%d"), params->redChannel->CutOff, params->redChannel->Contrast, params->redChannel->numLabels);
		m_Log.Message(message);

		for (int i = 0; i < params->redChannel->numLabels; i++)
		{
			message.Format(_T("Red Blobs: label=%d, PixelCount=%d"), params->redChannel->labels[i], params->redChannel->pixelCount[i]);
			m_Log.Message(message);
		}
		m_RGNDataArray[m_RgnIdx]->SetCutoffContrast(RED_COLOR, params->redChannel->CutOff, params->redChannel->Contrast);

		if (NULL == (params->greenChannel = GetImageParams(m_GreenRegionImage, m_ImageWidth, m_ImageHeight)))
		{
			break;
		}

		message.Format(_T("Green Parameters: Cutoff=%d, Contrast=%d, NumLabels=%d"), params->greenChannel->CutOff, params->greenChannel->Contrast, params->greenChannel->numLabels);
		m_Log.Message(message);

		for (int i = 0; i < params->greenChannel->numLabels; i++)
		{
			message.Format(_T("Green Blobs: label=%d, PixelCount=%d"), params->greenChannel->labels[i], params->greenChannel->pixelCount[i]);
			m_Log.Message(message);
		}

		m_RGNDataArray[m_RgnIdx]->SetCutoffContrast(GREEN_COLOR, params->greenChannel->CutOff, params->greenChannel->Contrast);

		if (NULL == (params->blueChannel = GetImageParams(m_BlueRegionImage, m_ImageWidth, m_ImageHeight)))
		{
			break;
		}

		message.Format(_T("Blue Parameters: Cutoff=%d, Contrast=%d, NumLabels=%d"), params->blueChannel->CutOff, params->blueChannel->Contrast, params->blueChannel->numLabels);
		m_Log.Message(message);

		for (int i = 0; i < params->blueChannel->numLabels; i++)
		{
			message.Format(_T("Blue Blobs: label=%d, PixelCount=%d"), params->blueChannel->labels[i], params->blueChannel->pixelCount[i]);
			m_Log.Message(message);
		}

		m_RGNDataArray[m_RgnIdx]->SetCutoffContrast(BLUE_COLOR, params->blueChannel->CutOff, params->blueChannel->Contrast);

		int overlayPixels = 0;
		if (m_WBCOnly.GetCheck() == BST_CHECKED)
		{
			overlayPixels = GetMaxIntersectPixels(params->greenChannel, params->blueChannel);
		}
		else
		{
			overlayPixels = GetMaxIntersectPixels(params->redChannel, params->blueChannel);
		}

		message.Format(_T("OverlayPixels=%d"), overlayPixels);
		m_Log.Message(message);

		if (m_SearchRed.GetCheck() == BST_CHECKED)
		{
			if (params->redChannel->numLabels > 0)
			{
				unsigned char *redBlobs = new unsigned char[params->redChannel->numLabels];
				memset(redBlobs, 0, sizeof(unsigned char) * params->redChannel->numLabels);
				unsigned char *ptrRed = params->redChannel->map;
				unsigned char *ptrBlue = params->blueChannel->map;
				for (int i = 0; i < m_ImageHeight; i++)
				{
					for (int j = 0; j < m_ImageWidth; j++)
					{
						if (ptrRed != NULL)
						{
							if (ptrBlue != NULL)
							{
								if ((*ptrRed > 0) && (*ptrBlue > 0))
								{
									redBlobs[*ptrRed - 1] = 1;
								}
								ptrBlue++;
							}
							else
							{
								if (*ptrRed > 0)
									redBlobs[*ptrRed - 1] = 1;
							}
							ptrRed++;
						}
					}
				}

				int mapPixelCount = 0;
				if (params->redChannel->map != NULL)
				{
					unsigned char *ptr = map;
					for (int i = 0; i < params->redChannel->numLabels; i++)
					{
						ptr = map;
						ptrRed = params->redChannel->map;
						if (redBlobs[i] == 1)
						{
							for (int k = 0; k < m_ImageHeight; k++)
							{
								for (int h = 0; h < m_ImageWidth; h++, ptr++, ptrRed++)
								{
									if (*ptrRed == params->redChannel->labels[i])
									{
										*ptr = (unsigned char)255;
										mapPixelCount++;
									}
								}
							}
						}
					}
				}
				*numPixels = mapPixelCount;
				delete redBlobs;
			}
		}
		else if (m_SearchGreen.GetCheck() == BST_CHECKED)
		{
			if (params->greenChannel->numLabels > 0)
			{
				unsigned char *greenBlobs = new unsigned char[params->greenChannel->numLabels];
				memset(greenBlobs, 0, sizeof(unsigned char) * params->greenChannel->numLabels);
				unsigned char *ptrGreen = params->greenChannel->map;
				unsigned char *ptrBlue = params->blueChannel->map;
				for (int i = 0; i < m_ImageHeight; i++)
				{
					for (int j = 0; j < m_ImageWidth; j++)
					{
						if (ptrGreen != NULL)
						{
							if (ptrBlue != NULL)
							{
								if ((*ptrGreen > 0) && (*ptrBlue > 0))
								{
									greenBlobs[*ptrGreen - 1] = 1;
								}
								ptrBlue++;
							}
							else
							{
								if (*ptrGreen > 0)
									greenBlobs[*ptrGreen - 1] = 1;
							}
							ptrGreen++;
						}
					}
				}

				int mapPixelCount = 0;
				if (params->greenChannel->map != NULL)
				{
					unsigned char *ptr = map;
					for (int i = 0; i < params->greenChannel->numLabels; i++)
					{
						ptr = map;
						ptrGreen = params->greenChannel->map;
						if (greenBlobs[i] == 1)
						{
							for (int k = 0; k < m_ImageHeight; k++)
							{
								for (int h = 0; h < m_ImageWidth; h++, ptr++, ptrGreen++)
								{
									if (*ptrGreen == params->greenChannel->labels[i])
									{
										*ptr = (unsigned char)255;
										mapPixelCount++;
									}
								}
							}
						}
					}
				}
				*numPixels = mapPixelCount;
				delete greenBlobs;
			}
		}
		else
		{
			if (params->blueChannel->numLabels > 0)
			{
				unsigned char *blueBlobs = new unsigned char[params->blueChannel->numLabels];
				memset(blueBlobs, 0, sizeof(unsigned char) * params->blueChannel->numLabels);
				unsigned char *ptrRed = params->redChannel->map;
				if (m_WBCOnly.GetCheck() == BST_CHECKED)
					ptrRed = params->greenChannel->map;
				unsigned char *ptrBlue = params->blueChannel->map;
				for (int i = 0; i < m_ImageHeight; i++)
				{
					for (int j = 0; j < m_ImageWidth; j++)
					{
						if (ptrBlue != NULL)
						{
							if (ptrRed != NULL)
							{
								if ((*ptrRed > 0) && (*ptrBlue > 0))
								{
									blueBlobs[*ptrBlue - 1] = 1;
								}
								ptrRed++;
							}
							else
							{
								if (*ptrBlue > 0)
									blueBlobs[*ptrBlue - 1] = 1;
							}
							ptrBlue++;
						}
					}
				}

				int mapPixelCount = 0;
				if (params->blueChannel->map != NULL)
				{
					unsigned char *ptr = map;
					for (int i = 0; i < params->blueChannel->numLabels; i++)
					{
						ptr = map;
						ptrBlue = params->blueChannel->map;
						if (blueBlobs[i] == 1)
						{
							for (int k = 0; k < m_ImageHeight; k++)
							{
								for (int h = 0; h < m_ImageWidth; h++, ptr++, ptrBlue++)
								{
									if (*ptrBlue == params->blueChannel->labels[i])
									{
										*ptr = (unsigned char)255;
										mapPixelCount++;
									}
								}
							}
						}
					}
				}
				*numPixels = mapPixelCount;
				delete blueBlobs;
			}
		}
		ret = true;
		break;
	}

	message.Format(_T("NumPixels=%d"), *numPixels);
	m_Log.Message(message);
	FreeHitImageParams(params);
	delete params;
	return ret;
}

void CCKIntensityCalculatorDlg::PaintCellRegionBoundary(unsigned char *image, int width, int height)
{
	unsigned char *tempImage = new unsigned char[width * height];
	unsigned char *tempImage1 = new unsigned char[width * height];
	memset(tempImage, 0, width * height);
	memset(tempImage1, 0, width * height);

	for (int i = 1; i < (height - 1); i++)
		for (int j = 1; j < (width - 1); j++)
		{
		bool found = false;
		for (int k = -1; k < 2; k++)
		{
			for (int h = -1; h < 2; h++)
			{
				if (*(image + width * (i + k) + (j + h)) == 255)
				{
					found = true;
					break;
				}

			}
			if (found)
				break;
		}
		if (found)
			*(tempImage + width * i + j) = 255;
		}

	for (int i = 1; i < (height - 1); i++)
		for (int j = 1; j < (width - 1); j++)
		{
			if (image[width * i + j] == tempImage[width * i + j])
				tempImage1[width * i + j] = 0;
			else
				tempImage1[width * i + j] = 255;
		}

	memcpy(image, tempImage1, width * height);

	delete[] tempImage;
	delete[] tempImage1;
}

void CCKIntensityCalculatorDlg::CopyGreenImage(unsigned char *image, int width, int height)
{
	if (m_CellRegionImage != NULL)
	{
		m_CellRegionImage.Destroy();
	}
	m_CellRegionImage.Create(width, -height, 24);
	BYTE *pCursor = (BYTE*)m_CellRegionImage.GetBits();
	BYTE *pGreenBuffer = image;
	BYTE *pRedBuffer = NULL;
	BYTE *pBlueBuffer = NULL;
	if (m_SearchRed.GetCheck() == BST_CHECKED)
	{
		pGreenBuffer = NULL;
		pRedBuffer = image;
		pBlueBuffer = NULL;
	}
	else if (m_SearchBlue.GetCheck() == BST_CHECKED)
	{
		pGreenBuffer = NULL;
		pRedBuffer = NULL;
		pBlueBuffer = image;
	}

	int nHeight = height;
	int nWidth = width;
	int nStride = m_CellRegionImage.GetPitch() - (nWidth * 3);

	for (int y = 0; y<nHeight; y++)
	{
		for (int x = 0; x<nWidth; x++)
		{
			if (pBlueBuffer != NULL)
				*pCursor++ = *pBlueBuffer++;
			else
				*pCursor++ = (BYTE)0;
			if (pGreenBuffer != NULL)
				*pCursor++ = *pGreenBuffer++;
			else
				*pCursor++ = (BYTE)0;
			if (pRedBuffer != NULL)
				*pCursor++ = *pRedBuffer++;
			else
				*pCursor++ = (BYTE)0;
		}
		if (nStride > 0)
			pCursor += nStride;
	}
}

void CCKIntensityCalculatorDlg::CopyCKImage(unsigned char *map)
{
	int width = m_RegionWidth;
	int height = m_RegionHeight;
	bool redDisplay = true;
	unsigned short *image = m_RedRegionImage;
	int cutoff = 0;
	int contrast = 0;
	if (m_WBCOnly.GetCheck() == BST_CHECKED)
	{
		image = m_GreenRegionImage;
		m_CKIntensityLabel = _T("CD Intensity (Green Intensity) Image");
		UpdateData(FALSE);
		redDisplay = false;
		m_RGNDataArray[m_RgnIdx]->GetCutoffContrast(GREEN_COLOR, &cutoff, &contrast);
	}
	else
	{
		m_CKIntensityLabel = _T("CK Intensity (Red Intensity) Image");
		UpdateData(FALSE);
		m_RGNDataArray[m_RgnIdx]->GetCutoffContrast(RED_COLOR, &cutoff, &contrast);
	}
	if (m_CKRegionImage != NULL)
	{
		m_CKRegionImage.Destroy();
	}
	m_CKRegionImage.Create(width, -height, 24);

	BYTE *pCursor = (BYTE*)m_CKRegionImage.GetBits();
	unsigned short *pRedBuffer = image;
	BYTE *pBoundary = map;
	int nHeight = height;
	int nWidth = width;
	int nStride = m_CellRegionImage.GetPitch() - (nWidth * 3);

	for (int y = 0; y<nHeight; y++)
	{
		for (int x = 0; x<nWidth; x++)
		{
			if (*pBoundary++ == (BYTE)255)
			{
				*pCursor++ = (BYTE)255;
				*pCursor++ = (BYTE)255;
				*pCursor++ = (BYTE)255;
				if (pRedBuffer != NULL)
					pRedBuffer++;
			}
			else
			{
				*pCursor++ = (BYTE)0;
				if (!(redDisplay) && (pRedBuffer != NULL))
					*pCursor++ = GetContrastEnhancedByte(*pRedBuffer++, cutoff, contrast);
				else
					*pCursor++ = (BYTE)0;
				if ((redDisplay) && (pRedBuffer != NULL))
					*pCursor++ = GetContrastEnhancedByte(*pRedBuffer++, cutoff, contrast);
				else
					*pCursor++ = (BYTE)0;
			}
		}
		if (nStride > 0)
			pCursor += nStride;
	}
}

void CCKIntensityCalculatorDlg::SumCKIntensity(unsigned short *redImage, unsigned char *greenMap, int width, int height, int *sum)
{
	*sum = 0;

	for (int i = 0; i < height; i++)
		for (int j = 0; j < width; j++)
			if (greenMap[width * i + j] == 255)
				*sum += redImage[width * i + j];
}

void CCKIntensityCalculatorDlg::OnBnClickedExport()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CString filenameForSave;

	CFileDialog dlg(FALSE,    // save
		CString(".csv"),    // no default extension
		NULL,    // no initial file name
		OFN_OVERWRITEPROMPT,
		_T("CSV files (*.csv)|*.csv||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		filenameForSave = dlg.GetPathName();
		char filename[512];
		size_t size;
		wcstombs_s(&size, filename, filenameForSave.GetBuffer(), filenameForSave.GetLength());
		FILE *pFile;
		fopen_s(&pFile, filename, "w");
		int totalPixels = 0;
		int totalCKs = 0;
		int totalGreens = 0;
		int totalBlues = 0;
		int totalRedPixels = 0;
		int totalBluePixels = 0;
		int totalCKAvg = 0;
		fprintf(pFile, "RegionIndex,RedStainPixels,RedIntensity,CK20Avg,RedCPI,GreenStainPixels,GreenIntensity,BlueStainPixels,BlueIntensity\n");
		int numCells = 0;
		int pixelCount = 0;
		for (int i = 0; i < (int)m_RGNDataArray.size(); i++)
		{
			if (m_SearchGreen.GetCheck() == BST_CHECKED)
				pixelCount = m_PixelCounts[i];
			else if (m_SearchRed.GetCheck() == BST_CHECKED)
				pixelCount = m_RedPixels[i];
			else
				pixelCount = m_BluePixels[i];
			if (pixelCount > 0)
			{
				numCells++;
				totalPixels += m_PixelCounts[i];
				totalCKs += m_CKData[i];
				totalGreens += m_GreenData[i];
				totalBlues += m_BlueData[i];
				totalRedPixels += m_RedPixels[i];
				totalBluePixels += m_BluePixels[i];
				int ckAvg = (m_CKData[i] / m_RedPixels[i]);
				totalCKAvg += ckAvg;
				fprintf(pFile, "%d,%d,%d,%d,%d,%d,%d,%d,%d\n", i + 1, m_RedPixels[i], m_CKData[i], ckAvg, m_RedTIFFData->GetCPI(), m_PixelCounts[i], m_GreenData[i], m_BluePixels[i], m_BlueData[i]);
			}
		}
		if (numCells > 0)
		{
			fprintf(pFile, "Total,%d,%d,%d,%d,%d,%d,%d,%d\n", totalRedPixels, totalCKs, totalCKAvg, (numCells * m_RedTIFFData->GetCPI()), totalRedPixels, totalPixels, totalGreens, totalBluePixels, totalBlues);
			fprintf(pFile, "Average,%d,%d,%d,%d,%d,%d,%d,%d\n", (int)(totalRedPixels + 0.01) / numCells, (int)(totalCKs + 0.01) / numCells, totalCKAvg / numCells, m_RedTIFFData->GetCPI(),
				(int)(totalPixels + 0.01) / numCells, (int)(totalGreens + 0.01) / numCells, (int)(totalBluePixels + 0.01) / numCells,
				(int)(totalBlues + 0.01) / numCells);
		}
		fclose(pFile);
		m_Status.Format(_T("%s saved"), dlg.GetFileName());
		UpdateData(FALSE);
	}
}

void CCKIntensityCalculatorDlg::CleanIntensityList()
{
	m_IntensityList.DeleteAllItems();
}

BOOL CCKIntensityCalculatorDlg::PreTranslateMessage(MSG* pMsg)
{
	CEdit *averageCK = (CEdit *)GetDlgItem(IDC_AVECK);
	CEdit *regionCount = (CEdit *)GetDlgItem(IDC_REGIONCOUNT);
	CEdit *regionIndex = (CEdit *)GetDlgItem(IDC_REGIONINDEX);
	CEdit *maxPixels = (CEdit *)GetDlgItem(IDC_MAXPIXEL);
	CEdit *minPixels = (CEdit *)GetDlgItem(IDC_MINPIXEL);

	if ((pMsg->message == WM_KEYDOWN) &&
		(pMsg->wParam == VK_RETURN) &&
		((GetFocus() == averageCK) ||
		(GetFocus() == regionCount) ||
		(GetFocus() == regionIndex) ||
		(GetFocus() == maxPixels) ||
		(GetFocus() == minPixels)))
	{
		return TRUE;
	}

	return CDialog::PreTranslateMessage(pMsg);
}

int CCKIntensityCalculatorDlg::SumPixelCount(unsigned char *map, int width, int height, unsigned short *image)
{
	int sum = 0;
	unsigned char *ptr1 = map;
	unsigned short *ptr2 = image;
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++, ptr1++, ptr2++)
		{
			if ((*ptr1 == 255) && (*ptr2 > 0))
				sum++;
		}
	}

	return sum;
}

BYTE CCKIntensityCalculatorDlg::GetContrastEnhancedByte(unsigned short value, int cutoff, int contrast)
{
	int value1 = value;
	value1 -= cutoff;
	if (value1 < 0)
		value1 = 0;

	if (value1 > contrast)
		value1 = contrast;

	return (BYTE)(255.0 * value1 / contrast);
}

void CCKIntensityCalculatorDlg::FreeHitImageParams(struct HitImageParams *params)
{
	if (params->redChannel != NULL)
	{
		FreeChannelImageParams(params->redChannel);
		delete params->redChannel;
		params->redChannel = NULL;
	}
	if (params->greenChannel != NULL)
	{
		FreeChannelImageParams(params->greenChannel);
		delete params->greenChannel;
		params->greenChannel = NULL;
	}
	if (params->blueChannel)
	{
		FreeChannelImageParams(params->blueChannel);
		delete params->blueChannel;
		params->blueChannel = NULL;
	}
}

void CCKIntensityCalculatorDlg::FreeChannelImageParams(struct ChannelImageParams *params)
{
	if (params->labels != NULL)
	{
		delete params->labels;
		params->labels = NULL;
	}
	if (params->map != NULL)
	{
		delete params->map;
		params->map = NULL;
	}
	if (params->pixelCount != NULL)
	{
		delete params->pixelCount;
		params->pixelCount = NULL;
	}
}

struct ChannelImageParams *CCKIntensityCalculatorDlg::GetImageParams(unsigned short *image, int width, int height)
{
	struct ChannelImageParams *params = new struct ChannelImageParams;
	memset(params, 0, sizeof(struct ChannelImageParams));
	int arraysize = MAXINT_12BIT;
	if (m_Zeiss.GetCheck() == BST_CHECKED)
		arraysize = MAXINT_14BIT;

	unsigned int *hist = new unsigned int[arraysize];
	memset(hist, 0, sizeof(unsigned int) * arraysize);

	unsigned short *ptr = image;

	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++, ptr++)
		{
			hist[(int)*ptr]++;
		}
	}

	CString message;
	unsigned short threshold = 0;
	int count = 0;

	for (int i = arraysize - 1; i > 1; i--)
	{
		count += hist[i];
		if ((params->Contrast == 0) && (count >= 5))
		{
			params->Contrast = i;
			message.Format(_T("count=%d, params->Contrast=%d"), count, params->Contrast);
			m_Log.Message(message);
		}
		if ((threshold == 0) && (count >= m_MaxPixels))
		{
			threshold = (unsigned short)i;
			message.Format(_T("count=%d, Initial threshold=%u"), count, threshold);
			m_Log.Message(message);
		}
		if (count >= 6000)
		{
			params->CutOff = i;
			message.Format(_T("count=%d, params->Cutoff=%d"), count, params->CutOff);
			m_Log.Message(message);
			break;
		}
		message = _T("");
	}
	delete hist;

	CLabelConnectedPixels m_Grouper;
	if ((params->Contrast - params->CutOff) > 10)
	{
		int numLabels = 0;
		unsigned short *labels = NULL;
		int *pixelCount = NULL;
		unsigned short *map1 = NULL;
		double maxExtent = 0;
		double sumOfExtent = 0;
		unsigned short minThreshold = 0;
		bool extentUpdated = false;
		bool ret = false;

		while (true)
		{
			ret = m_Grouper.LabelConnectedComponents(image, width, height, threshold, &numLabels, &labels, &pixelCount, &map1);
			if (ret)
			{
				sumOfExtent = 0.0;
				for (int j = 0; j < numLabels; j++)
				{
					if (pixelCount[j] >= m_MinPixels)
					{
						unsigned short *ptr1 = map1;
						int x0 = width;
						int x1 = 0;
						int y0 = height;
						int y1 = 0;
						for (int k = 0; k < height; k++)
						{
							for (int h = 0; h < width; h++, ptr1++)
							{
								if (*ptr1 == labels[j])
								{
									if (h < x0)
										x0 = h;
									if (h > x1)
										x1 = h;
									if (k < y0)
										y0 = k;
									if (k > y1)
										y1 = k;
								}
							}
						}
						double extent = ((double)pixelCount[j]) / (y1 - y0 + 1) / (x1 + x0 + 1);
						sumOfExtent += extent;
						message.Format(_T("i=%d, x0=%d, y0=%d, x1=%d, y1=%d, pixelCount=%d, extent=%0.2lf, sumOfExtent=%.2lf"), j, x0, y0, x1, y1, pixelCount[j], extent, sumOfExtent);
						m_Log.Message(message);
					}
				}

				if (sumOfExtent > maxExtent)
				{
					minThreshold = threshold;
					maxExtent = sumOfExtent;
					extentUpdated = true;
				}
				message.Format(_T("threshold=%d, minThreshold=%d, maxExtent=%0.2lf, sumOfExtent=%0.2lf, extenUpdated=%d"), threshold, minThreshold, maxExtent, sumOfExtent, extentUpdated);
				m_Log.Message(message);
			}
			else
			{
				message.Format(_T("LabelConnectedComponents() returned fail."));
				m_Log.Message(message);
			}
			if (labels != NULL)
			{
				delete labels;
				labels = NULL;
			}
			if (pixelCount != NULL)
			{
				delete pixelCount;
				pixelCount = NULL;
			}
			if (map1 != NULL)
			{
				delete map1;
				map1 = NULL;
			}
			numLabels = 0;

			if (extentUpdated)
			{
				threshold += 6;
				extentUpdated = false;
				if (threshold >= params->Contrast)
					break;
			}
			else
			{
				break;
			}
		}

		threshold = minThreshold;
		ret = m_Grouper.LabelConnectedComponents(image, width, height, threshold, &numLabels, &labels, &pixelCount, &map1);
		if (ret)
		{
			message.Format(_T("Final: threshold=%d, minThreshold=%d, numLabels=%d"), threshold, minThreshold, numLabels);
			m_Log.Message(message);
			if (numLabels > 0)
			{
				params->map = new unsigned char[width * height];
				memset(params->map, 0, sizeof(unsigned char) * width * height);
				params->labels = new unsigned char[numLabels];
				memset(params->labels, 0, sizeof(unsigned char) * numLabels);
				params->pixelCount = new int[numLabels];
				memset(params->pixelCount, 0, sizeof(int) * numLabels);
				unsigned char currentLabel = 0;
				int labelIndex = 0;
				int pixelCounts = 0;
				for (int i = 0; i < numLabels; i++)
				{
					if (pixelCount[i] >= m_MinPixels)
					{
						unsigned short *ptr1 = map1;
						unsigned char *ptr = params->map;
						if (currentLabel == 255)
						{
							message.Format(_T("Label exceeds 255"));
							m_Log.Message(message);
							break;
						}
						currentLabel++;
						params->labels[labelIndex] = currentLabel;
						pixelCounts = 0;
						for (int k = 0; k < height; k++)
						{
							for (int h = 0; h < width; h++, ptr1++, ptr++)
							{
								if (*ptr1 == labels[i])
								{
									*ptr = currentLabel;
									pixelCounts++;
								}
							}
						}
						params->pixelCount[labelIndex] = pixelCounts;
						labelIndex++;
					}
				}
				params->numLabels = labelIndex;
			}
		}

		if (labels != NULL)
		{
			delete labels;
			labels = NULL;
		}
		if (pixelCount != NULL)
		{
			delete pixelCount;
			pixelCount = NULL;
		}
		if (map1 != NULL)
		{
			delete map1;
			map1 = NULL;
		}
	}

	return params;
}

int CCKIntensityCalculatorDlg::GetMaxIntersectPixels(struct ChannelImageParams *redChannel, struct ChannelImageParams *blueChannel)
{
	int maxCount = 0;

	unsigned char *ptrRed = redChannel->map;
	unsigned char *ptrBlue = blueChannel->map;

	if ((ptrRed != NULL) && (ptrBlue != NULL))
	{
		for (int i = 0; i < m_ImageHeight; i++)
		{
			for (int j = 0; j < m_ImageWidth; j++, ptrRed++, ptrBlue++)
			{
				if ((*ptrRed > 0) && (*ptrBlue > 0))
					maxCount++;
			}
		}
	}
	return maxCount;
}

BOOL CCKIntensityCalculatorDlg::LoadRegionData(CString filename)
{
	BOOL ret = FALSE;
	int failureLocation = 0;
	CFile theFile;
	CString message;
	message.Format(_T("Start to Load %s"), filename);
	m_Log.Message(message);
	UINT32 color = 0;
	BYTE red = 0;
	BYTE blue = 0;
	BYTE green = 0;
	int index = 0;

	try
	{
		if (theFile.Open(filename, CFile::modeRead))
		{
			failureLocation = 1;
			BYTE* buf = new BYTE[512];
			int count = 0;
			int offset = 0;
			char *ptr = NULL;
			char *ptr1 = NULL;
			int x0 = 0;
			int y0 = 0;

			while (TRUE)
			{
				failureLocation = 2;
				count = theFile.Read(&buf[offset], 1);
				if (count == 1)
				{
					failureLocation = 3;
					if ((buf[offset] == '\0') || (buf[offset] == '\n') || (buf[offset] == '\r'))
					{
						failureLocation = 4;
						if (offset > 0)
						{
							failureLocation = 5;
							if (filename.Find(_T(".rgn"), 0) > 0)
							{
								ptr = strchr((char *)buf, ',');
								if (ptr != NULL)
								{
									failureLocation = 6;
									ptr = strstr(ptr, " 1 ");
									if (ptr != NULL)
									{
										ptr += 3;
										ptr1 = strchr(ptr, ',');
										if (ptr1 != NULL)
										{
											failureLocation = 7;
											*ptr1 = '\0';
											color = (UINT32)atoi(ptr);
											red = (BYTE)(color & 0xFF);
											green = (BYTE)((color >> 8) & 0xFF);
											blue = (BYTE)((color >> 16) & 0xFF);
											ptr1++;
											ptr = strstr(ptr1, " 2 ");
											if (ptr != NULL)
											{
												failureLocation = 8;
												ptr += 3;
												ptr1 = strchr(ptr, ' ');
												if (ptr1 != NULL)
												{
													failureLocation = 9;
													*ptr1 = '\0';
													x0 = atoi(ptr);
													ptr1++;
													ptr = strchr(ptr1, ',');
													if (ptr != NULL)
													{
														failureLocation = 10;
														*ptr = '\0';
														y0 = atoi(ptr1);
														int CenterX = x0 + 50;
														int CenterY = y0 + 50;
														message.Format(_T("RegionNo.%d, X0=%d, Y0=%d"), index + 1, x0, y0);
														m_Log.Message(message);
														CRGNData* data = new CRGNData(++index, blue, green, red, CenterX, CenterY);
														m_RGNDataArray.push_back(data);
														ret = TRUE;
														offset = 0;
														failureLocation = 11;
													}
												}
											}
										}
									}
								}
							}
							else
							{
								failureLocation = 8;
								int type = 0;
								buf[offset + 1] = '\0';
								ptr1 = NULL;
								ptr = strstr((char *)buf, "<Left>");
								if (ptr != NULL)
									type = 1;
								else
								{
									ptr = strstr((char *)buf, "<Top>");
									if (ptr != NULL)
										type = 2;
									else
									{
										failureLocation = 9;
										type = 0;
										if (buf[offset] == '\0')
											break;
										else
										{
											offset = 0;
											continue;
										}
									}
								}
								if (type == 1)
								{
									failureLocation = 10;
									ptr += 6;
									ptr1 = strchr(ptr, '<');
									*ptr1 = '\0';
									x0 = (int)atof(ptr);
									offset = 0;
									continue;
								}
								else if (type == 2)
								{
									failureLocation = 11;
									ptr += 5;
									ptr1 = strchr(ptr, '<');
									*ptr1 = '\0';
									y0 = (int)atof(ptr);
									red = 255;
									green = 0;
									blue = 0;
									int CenterX = x0 + 50;
									int CenterY = y0 + 50;
									message.Format(_T("RegionNo.%d, X0=%d, Y0=%d"), index + 1, x0, y0);
									m_Log.Message(message);
									CRGNData* data = new CRGNData(++index, blue, green, red, CenterX, CenterY);
									m_RGNDataArray.push_back(data);
									ret = TRUE;
									offset = 0;
									continue;
								}
							}
						}
						else
						{
							if (buf[offset] == '\0')
							{
								failureLocation = 12;
								break;
							}
							else
							{
								failureLocation = 13;
								offset = 0;
								continue;
							}
						}
					}
					else
					{
						failureLocation = 14;
						offset++;
					}
				}
				else
				{
					failureLocation = 15;
					break;
				}
			}

			theFile.Close();
			delete[] buf;
			buf = NULL;
		}
	}
	catch (CException *e)
	{
		TCHAR errCause[255];
		e->GetErrorMessage(errCause, 255);
		CString msg;
		msg.Format(_T("Cought Exception %s, Failre Location = %d"), errCause, failureLocation);
		m_Log.Message(msg);
		ret = FALSE;
	}
	catch (...)
	{
		CString msg;
		msg.Format(_T("Cought Exception, Failre Location = %d"), failureLocation);
		m_Log.Message(msg);
		ret = FALSE;
	}
	if (!ret)
		m_Status = _T("Failed to load Region Data from File");
	UpdateData(FALSE);
	return ret;
}

void CCKIntensityCalculatorDlg::LoadDefaultSettings()
{
	CStdioFile theFile;
	if (theFile.Open(_T(".\\DefaultSettings.txt"), CFile::modeRead | CFile::typeText))
	{
		CString textline;
		while (theFile.ReadString(textline))
		{
			if (textline.Find(_T("Leica_Red_Prefix=")) > -1)
			{
				CString tag = _T("Leica_Red_Prefix=");
				Leica_Red_Prefix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Leica_Green_Prefix=")) > -1)
			{
				CString tag = _T("Leica_Green_Prefix=");
				Leica_Green_Prefix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Leica_Blue_Prefix=")) > -1)
			{
				CString tag = _T("Leica_Blue_Prefix=");
				Leica_Blue_Prefix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Zeiss_Red_Postfix=")) > -1)
			{
				CString tag = _T("Zeiss_Red_Postfix=");
				Zeiss_Red_Postfix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Zeiss_Green_Postfix=")) > -1)
			{
				CString tag = _T("Zeiss_Green_Postfix=");
				Zeiss_Green_Postfix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Zeiss_Blue_Postfix=")) > -1)
			{
				CString tag = _T("Zeiss_Blue_Postfix=");
				Zeiss_Blue_Postfix = textline.Mid(tag.GetLength());
			}
		}
		theFile.Close();
	}
}
