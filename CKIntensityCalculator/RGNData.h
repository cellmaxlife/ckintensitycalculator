#pragma once

#include "ColorType.h"

class CRGNData
{
protected:
	int m_Index;
	unsigned char m_Blue;
	unsigned char m_Green;
	unsigned char m_Red;
	int m_CenterX;
	int m_CenterY;
	int m_RedCutoff;
	int m_RedContrast;
	int m_GreenCutoff;
	int m_GreenContrast;
	int m_BlueCutoff;
	int m_BlueContrast;

public:
	CRGNData(int index, unsigned char blue, unsigned char green, unsigned char red, int CenterX, int CenterY);
	virtual ~CRGNData();
	bool GetOneItem(int index, unsigned char *blue, unsigned char *green, unsigned char *red, int *Center, int *CenterY);
	void SetCutoffContrast(PIXEL_COLOR_TYPE color, int cutoff, int contrast);
	void GetCutoffContrast(PIXEL_COLOR_TYPE color, int *cutoff, int *contrast);
};