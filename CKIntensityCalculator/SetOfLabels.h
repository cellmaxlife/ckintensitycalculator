#pragma once
#include <vector>

using namespace std;

class CSetOfLabels
{
public:
	CSetOfLabels();
	virtual ~CSetOfLabels();
	
	void Add(unsigned char label);
	void Clear();
	vector<unsigned char> GetLabels();
	int GetLabel(int key);

protected:
	vector<unsigned char> m_LabelSet;
};
