#include "stdafx.h"
#include "SetOfLabels.h"

CSetOfLabels::CSetOfLabels()
{
}

CSetOfLabels::~CSetOfLabels()
{
	m_LabelSet.clear();
}

void CSetOfLabels::Add(unsigned char label)
{
	m_LabelSet.push_back(label);
}

void CSetOfLabels::Clear()
{
	m_LabelSet.clear();
}

vector<unsigned char> CSetOfLabels::GetLabels()
{
	return m_LabelSet;
}

int CSetOfLabels::GetLabel(int key)
{
	int value = key;
	for (int i = 0; i < (int)m_LabelSet.size(); i++)
	{
		int value1 = (int)m_LabelSet[i];
		if (value1 < value)
			value = value1;
	}
	return value;
}