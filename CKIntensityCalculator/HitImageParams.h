#pragma once

struct HitImageParams
{
public:
	struct ChannelImageParams *redChannel;
	struct ChannelImageParams *greenChannel;
	struct ChannelImageParams *blueChannel;
};

struct ChannelImageParams
{
public:
	int CutOff;
	int Contrast;
	int threshold;
	unsigned char *map;
	int numLabels;
	unsigned char *labels;
	int *pixelCount;
};


