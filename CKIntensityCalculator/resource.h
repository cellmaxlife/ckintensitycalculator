//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 產生的 Include 檔案。
// 由 CKIntensityCalculator.rc 使用
//
#define IDD_CKINTENSITYCALCULATOR_DIALOG 102
#define IDR_MAINFRAME                   128
#define IDC_RGBImage                    1000
#define IDC_CKIMAGE                     1001
#define IDC_2REGION                     1002
#define IDC_BLUE                        1004
#define IDC_GREEN                       1005
#define IDC_RED                         1006
#define IDC_OVERLAY                     1007
#define IDC_REGION                      1008
#define IDC_STATUS                      1009
#define IDC_REGIONCOUNT                 1010
#define IDC_REGIONINDEX                 1011
#define IDC_SELECT                      1012
#define IDC_NEXT                        1013
#define IDC_PREV                        1014
#define IDC_OPENTIFF                    1016
#define IDC_OPENRGN                     1017
#define IDC_CALCULATE                   1018
#define IDC_INTENSITYLIST               1019
#define IDC_EDIT1                       1020
#define IDC_AVECK                       1020
#define IDC_BUTTON1                     1021
#define IDC_EXPORT                      1021
#define IDC_CTCONLY                     1022
#define IDC_RADIO2                      1023
#define IDC_ALLTYPES                    1023
#define IDC_SGREEN                      1024
#define IDC_SRED                        1025
#define IDC_SGREEN2                     1026
#define IDC_SBLUE                       1026
#define IDC_MINPIXEL                    1027
#define IDC_MINPIXEL2                   1028
#define IDC_MAXPIXEL                    1028
#define IDC_CTCONLY2                    1029
#define IDC_WBCONLY                     1029
#define IDC_CKINTENLABEL                1030
#define IDC_LEICA                       1031
#define IDC_SGREEN3                     1032
#define IDC_ZEISS                       1032
#define IDC_EDIT2                       1033
#define IDC_AVERAGECD45                 1033

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1034
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
