
// CKIntensityCalculatorDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "SingleChannelTIFFData.h"
#include "RGNData.h"
#include <vector>
#include "afxcmn.h"
#include "LabelConnectedPixels.h"
#include "Log.h"
#include "HitImageParams.h"

using namespace std;

// CCKIntensityCalculatorDlg dialog
class CCKIntensityCalculatorDlg : public CDialogEx
{
// Construction
public:
	CCKIntensityCalculatorDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_CKINTENSITYCALCULATOR_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;
	CSingleChannelTIFFData *m_RedTIFFData;
	CSingleChannelTIFFData *m_GreenTIFFData;
	CSingleChannelTIFFData *m_BlueTIFFData;
	CImage m_Image;
	int m_ImageWidth;
	int m_ImageHeight;
	void CopyToRGBImage(unsigned short *pBlueBuffer, unsigned short *pGreenBuffer, unsigned short *pRedBuffer,
		int redCutoff, int redContrast, int greenCutoff, int greenContrast, int blueCutoff, int blueContrast);
	vector<CRGNData*> m_RGNDataArray;
	void FreeRGNData(void);
	BOOL DisplayROI(int index);
	int m_DisplayedRgnIndex;
	void ClickColor();
	BOOL LoadRegionData(CString pathname);
	bool GetRegionPixelMap(unsigned char *map, int *numPixels);
	CLabelConnectedPixels *m_Grouper;
	CImage m_CellRegionImage;
	CImage m_CKRegionImage;
	void CopyGreenImage(unsigned char *image, int width, int height);
	void CopyCKImage(unsigned char *map);
	unsigned char *m_GreenRegionMap;
	unsigned char *m_GreenRegionBoundary;
	void FreeImages();
	void PaintCellRegionBoundary(unsigned char *image, int width, int height);
	void SumCKIntensity(unsigned short *redImage, unsigned char *greenMap, int width, int height, int *sum);
	int* m_CKData;
	int* m_GreenData;
	int* m_PixelCounts;
	int* m_RedPixels;
	int* m_BluePixels;
	int* m_BlueData;
	void CleanIntensityList();
	int SumPixelCount(unsigned char *map, int width, int height, unsigned short *image);
	BYTE GetContrastEnhancedByte(unsigned short value, int cutoff, int contrast);
	int m_RegionX0;
	int m_RegionY0;
	int m_RegionWidth;
	int m_RegionHeight;
	int m_RgnIdx;
	CLog m_Log;
	unsigned short *m_RedRegionImage;
	unsigned short *m_GreenRegionImage;
	unsigned short *m_BlueRegionImage;
	void FreeHitImageParams(struct HitImageParams *params);
	void FreeChannelImageParams(struct ChannelImageParams *params);
	struct ChannelImageParams *GetImageParams(unsigned short *image, int width, int height);
	int GetMaxIntersectPixels(struct ChannelImageParams *redChannel, struct ChannelImageParams *blueChannel);

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CButton m_Blue;
	CButton m_Green;
	CButton m_Red;
	CStatic m_2RegionDisplay;
	CStatic m_CKIntensityDisplay;
	CStatic m_RGBImageDisplay;
	int m_RegionCount;
	CEdit m_RegionIndex;
	CString m_TIFFFilename;
	CString m_RegionFilename;
	CString m_Status;
	afx_msg void OnBnClickedOpentiff();
	afx_msg void OnBnClickedOpenrgn();
	afx_msg void OnBnClickedSelect();
	afx_msg void OnBnClickedNext();
	afx_msg void OnBnClickedPrev();
	afx_msg void OnBnClickedBlue();
	afx_msg void OnBnClickedGreen();
	afx_msg void OnBnClickedRed();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedCalculate();
	CListCtrl m_IntensityList;
	afx_msg void OnNMDblclkIntensitylist(NMHDR *pNMHDR, LRESULT *pResult);
	int m_AveCK;
	afx_msg void OnBnClickedExport();
	BOOL PreTranslateMessage(MSG* pMsg);
	CButton m_CTCOnly;
	CButton m_Alltypes;
	CButton m_SearchRed;
	CButton m_SearchGreen;
	CButton m_SearchBlue;
	int m_MinPixels;
	int m_MaxPixels;
	CString m_CKIntensityLabel; \
	CButton m_WBCOnly;
	CButton m_Leica;
	CButton m_Zeiss;
	int m_AvgCD45;
	CString Leica_Red_Prefix;
	CString Leica_Green_Prefix;
	CString Leica_Blue_Prefix;
	CString Zeiss_Red_Postfix;
	CString Zeiss_Green_Postfix;
	CString Zeiss_Blue_Postfix;
	void LoadDefaultSettings();
	bool m_ZeissData;
};
