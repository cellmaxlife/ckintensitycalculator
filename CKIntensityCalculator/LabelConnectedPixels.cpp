#include "stdafx.h"
#include "LabelConnectedPixels.h"
#include <string>
#include <vector>

using namespace std;

CLabelConnectedPixels::CLabelConnectedPixels()
{
	m_KeyLabelTable = NULL;
}

CLabelConnectedPixels::~CLabelConnectedPixels()
{
	CleanLabelTable();
}

void CLabelConnectedPixels::CleanLabelTable()
{
	while (m_KeyLabelTable != NULL)
	{
		struct LABELENTRY *labelPtr = m_KeyLabelTable->labelTable;
		m_KeyLabelTable->labelTable = NULL;
		while (labelPtr != NULL)
		{
			struct LABELENTRY *labelPtr1 = labelPtr->nextLabel;
			delete labelPtr;
			labelPtr = labelPtr1;
		}
		struct KEYENTRY *ptr = m_KeyLabelTable->nextKey;
		delete m_KeyLabelTable;
		m_KeyLabelTable = ptr;
	}
}

bool CLabelConnectedPixels::LabelConnectedComponents(unsigned short *image, int width, int height, unsigned short threshold, int *numLabels, unsigned short **labels, int **pixelCount, unsigned short **map)
{
	bool ret = false;
	
	int size = width * height;
	unsigned short *m_LabelImage = new unsigned short[size];
	memset(m_LabelImage, 0, size * sizeof(unsigned short));

	CleanLabelTable();

	unsigned short currentLabel = 0;

	// Pass #1
	try
	{
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				if (image[width * i + j] < threshold)
				{
					continue;
				}
				else if ((j == 0) && (i == 0))
				{
					if (currentLabel == MAXUINT16)
						throw 255;
					currentLabel++;
					m_LabelImage[0] = currentLabel;
					AddLabelEquivalence((int)currentLabel, (int)currentLabel);
				}
				else if ((i == 0) && (j > 0) && (image[(j - 1)] >= threshold))
				{
					m_LabelImage[j] = m_LabelImage[j - 1];
				}
				else if ((j == 0) && (i > 0) && (image[width * (i - 1)] >= threshold))
				{
					m_LabelImage[width * i] = m_LabelImage[width * (i - 1)];
				}
				else if (((i > 0) && (j > 0) && (image[width * i + (j - 1)] >= threshold))
					|| ((i > 0) && (j > 0) && (image[width * (i - 1) + (j - 1)] >= threshold))
					|| ((i > 0) && (image[width * (i - 1) + j] >= threshold))
					|| ((i > 0) && (j < (width-1)) && (image[width * (i - 1) + (j + 1)] >= threshold)))
				{
					if ((i > 0) && (j > 0) && (image[width * i + (j - 1)] >= threshold))
					{
						m_LabelImage[width * i + j] = m_LabelImage[width * i + (j - 1)];
					}
					if ((i > 0) && (j > 0) && (image[width * (i - 1) + (j - 1)] >= threshold))
					{
						if (m_LabelImage[width * i + j] == 0)
						{
							m_LabelImage[width * i + j] = m_LabelImage[width * (i - 1) + (j - 1)];
						}
						else
						{
							AddLabelEquivalence((int)m_LabelImage[width * i + j], (int)m_LabelImage[width * (i - 1) + (j - 1)]);
						}
					}
					if ((i > 0) && (image[width * (i - 1) + j] >= threshold))
					{
						if (m_LabelImage[width * i + j] == 0)
						{
							m_LabelImage[width * i + j] = m_LabelImage[width * (i - 1) + j];
						}
						else
						{
							AddLabelEquivalence((int)m_LabelImage[width * i + j], (int)m_LabelImage[width * (i - 1) + j]);
						}
					}
					if ((i > 0) && (j < (width-1)) && (image[width * (i - 1) + (j + 1)] >= threshold))
					{
						if (m_LabelImage[width * i + j] == 0)
						{
							m_LabelImage[width * i + j] = m_LabelImage[width * (i - 1) + (j + 1)];
						}
						else
						{
							AddLabelEquivalence((int)m_LabelImage[width * i + j], (int)m_LabelImage[width * (i - 1) + (j + 1)]);
						}
					}
				}
				else
				{
					if (currentLabel == MAXUINT)
						throw 255;
					currentLabel++;
					m_LabelImage[width * i + j] = currentLabel;
					AddLabelEquivalence((int)currentLabel, (int)currentLabel);
				}
			}
		}
	}
	catch (...)
	{
		delete m_LabelImage;
		return false;
	}

	// Pass #2
	for (int i = 0; i<height; i++)
		for (int j = 0; j<width; j++)
			m_LabelImage[width * i + j] = GetEquivalence(m_LabelImage[width * i + j]);
	
	vector<unsigned short> assignedLabels;
	vector<int> pixelCounts;

	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			unsigned short label1 = m_LabelImage[width * i + j];
			if (label1 >(unsigned short)0)
			{
				bool found = false;
				int index = 0;
				for (int k = 0; k < (int)assignedLabels.size(); k++)
				{
					if (assignedLabels[k] == label1)
					{
						found = true;
						index = k;
						break;
					}
				}
				if (found)
				{
					pixelCounts[index]++;
				}
				else
				{
					assignedLabels.push_back(label1);
					pixelCounts.push_back((unsigned short)1);
				}
			}
		}
	}

	if (assignedLabels.size() > 0)
	{
		ret = true;
		*numLabels = (int) assignedLabels.size();
		*labels = new unsigned short[assignedLabels.size()];
		*pixelCount = new int[assignedLabels.size()];
		for (int i = 0; i < (int)assignedLabels.size(); i++)
		{
			(*labels)[i] = assignedLabels[i];
			(*pixelCount)[i] = pixelCounts[i];
		}
		*map = m_LabelImage;
	}

	assignedLabels.clear();
	pixelCounts.clear();

	CleanLabelTable();

	return ret;
}

unsigned short CLabelConnectedPixels::GetEquivalence(unsigned short key)
{
	unsigned short label = 0;
	struct KEYENTRY *ptr = m_KeyLabelTable;
	while (ptr != NULL)
	{
		if (ptr->key == key)
		{
			label = ptr->minLabel;
			break;
		}
		else
			ptr = ptr->nextKey;
	}

	return label;
}

void CLabelConnectedPixels::AddLabelEquivalence(unsigned short key, unsigned short label)
{
	if (m_KeyLabelTable == NULL)
	{
		m_KeyLabelTable = new struct KEYENTRY;
		m_KeyLabelTable->nextKey = NULL;
		m_KeyLabelTable->key = key;
		m_KeyLabelTable->minLabel = label;
		m_KeyLabelTable->labelTable = new struct LABELENTRY;
		m_KeyLabelTable->labelTable->nextLabel = NULL;
		m_KeyLabelTable->labelTable->label = label;
	}
	else
	{
		struct KEYENTRY *ptr = m_KeyLabelTable;
		bool found = false;
		while (ptr != NULL)
		{
			if (ptr->key == key)
			{
				found = true;
				bool found1 = false;
				struct LABELENTRY *labelPtr = ptr->labelTable;
				while (labelPtr != NULL)
				{
					if (labelPtr->label == label)
					{
						found1 = true;
						break;
					}
					else
						labelPtr = labelPtr->nextLabel;
				}
				if (!found1)
				{
					if (label < ptr->minLabel)
						ptr->minLabel = label;
					struct LABELENTRY *data = new struct LABELENTRY;
					data->label = label;
					data->nextLabel = ptr->labelTable;
					ptr->labelTable = data;
				}
				break;
			}
			else
				ptr = ptr->nextKey;
		}
		if (!found)
		{
			struct KEYENTRY *data = new struct KEYENTRY;
			data->key = key;
			data->minLabel = label;
			data->labelTable = new struct LABELENTRY;
			data->labelTable->label = label;
			data->labelTable->nextLabel = NULL;
			data->nextKey = m_KeyLabelTable;
			m_KeyLabelTable = data;
			ptr = m_KeyLabelTable;
		}
		struct KEYENTRY *keyPtr = m_KeyLabelTable;
		while (keyPtr != NULL)
		{
			if (keyPtr->key == label)
			{
				if (keyPtr->minLabel < ptr->minLabel)
					ptr->minLabel = keyPtr->minLabel;
				struct LABELENTRY *labelPtr = keyPtr->labelTable;
				while (labelPtr != NULL)
				{
					struct KEYENTRY *keyPtr1 = m_KeyLabelTable;
					while (keyPtr1 != NULL)
					{
						if (keyPtr1->key == labelPtr->label)
						{
							if (keyPtr1->minLabel < ptr->minLabel)
								ptr->minLabel = keyPtr1->minLabel;
							break;
						}
						else
							keyPtr1 = keyPtr1->nextKey;
					}
					labelPtr = labelPtr->nextLabel;
				}
				break;
			}
			else
				keyPtr = keyPtr->nextKey;
		}
	}
}
